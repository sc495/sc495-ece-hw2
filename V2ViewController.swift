//
//  V2ViewController.swift
//  Project00
//
//  Created by student on 2/1/17.
//  Copyright © 2017 Siyang Chen. All rights reserved.
//

import UIKit

class V2ViewController: UIViewController {
    
    
    @IBOutlet weak var Show: UILabel!
    var ShowText:String=""
    
    @IBOutlet weak var ImagePho: UIImageView!
    var PhotoImg=UIImage(named: "Default")

    override func viewDidLoad() {
        //super.viewDidLoad()
        Show.text=ShowText
        ImagePho.image=PhotoImg
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
