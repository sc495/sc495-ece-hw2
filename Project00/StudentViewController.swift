//
//  ViewController.swift
//  Project00
//
//  Created by Siyang Chen on 1/30/17.
//  Copyright © 2017 Siyang Chen. All rights reserved.
//

import UIKit


class Person{
    var FirstName:String
    var LastName:String
    var FromWhere:String
    var Gender:String    //Male or Female
    var Hobby:String
    
    init(FirstName:String,LastName:String,FromWhere:String,Gender:String,Hobby:String){
    
        self.FirstName=FirstName
        self.LastName=LastName
        self.FromWhere=FromWhere
        self.Gender=Gender
        self.Hobby=Hobby
    }
    
}

class Student:Person,CustomStringConvertible{
    var DegreeWorkOn:String
    var ComputerLanguage:String
    
    init(FirstName:String,LastName:String,FromWhere:String,Gender:String,Hobby:String,DegreeWorkOn:String,ComputerLanguage:String) {
        self.DegreeWorkOn=DegreeWorkOn
        self.ComputerLanguage=ComputerLanguage
        super.init(FirstName: FirstName, LastName: LastName, FromWhere: FromWhere, Gender: Gender, Hobby: Hobby)
    }
    
    
    var description: String{
        var sentences=""
        if(self.Gender=="Female"){
            sentences+="\(self.FirstName) \(self.LastName), \(self.Gender) is from \(self.FromWhere). Her major is \(self.DegreeWorkOn) and she enjoys \(self.Hobby) outside of class. Oh, forget to mention, her proficient computer language is \(self.ComputerLanguage)."
        }
        else{
            sentences+="\(self.FirstName) \(self.LastName), \(self.Gender) is from \(self.FromWhere). His major is \(self.DegreeWorkOn) and he enjoys \(self.Hobby) outside of class. Oh, forget to mention, his proficient computer language is \(self.ComputerLanguage)."
        }
        return sentences
    }
}








class StudentViewController: UIViewController,UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    //MARK:Attributes
    @IBOutlet weak var FirstName: UITextField!
    @IBOutlet weak var LastName: UITextField!
    @IBOutlet weak var FromWhere: UITextField!
    @IBOutlet weak var Gender: UITextField!
    @IBOutlet weak var Hobby: UITextField!
    @IBOutlet weak var DegreeWorkOn: UITextField!
    @IBOutlet weak var ComputerLanguage: UITextField!
    
    //TextView For Description
    @IBOutlet weak var Output: UILabel!
    //PhotoTaken
    @IBOutlet weak var Photo: UIImageView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        FirstName.delegate=self
        LastName.delegate=self
        FromWhere.delegate=self
        Gender.delegate=self
        Hobby.delegate=self
        DegreeWorkOn.delegate=self
        ComputerLanguage.delegate=self
    }

    
    
    //MARK:UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //Hide keyboard
        FirstName.resignFirstResponder()
        LastName.resignFirstResponder()
        FromWhere.resignFirstResponder()
        Gender.resignFirstResponder()
        Hobby.resignFirstResponder()
        DegreeWorkOn.resignFirstResponder()
        ComputerLanguage.resignFirstResponder()
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {

    }
    

    //MARK: UIImagePickerControllerDelegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        Photo.image = selectedImage
        dismiss(animated: true, completion: nil)
    }
    
    
    
    
    //MARK:Actions
    
    @IBAction func Takephoto(_ sender: UITapGestureRecognizer) {
        let imagePick = UIImagePickerController()
        imagePick.sourceType = .camera
        imagePick.delegate = self
        present(imagePick, animated: true, completion: nil)
    }

    @IBAction func Register(_ sender: UIButton) {
        print("Default Text Button Tapped")
        
        
        let FN=FirstName.text
        let LN=LastName.text
        let FW=FromWhere.text
        let SEX=Gender.text
        let HB=Hobby.text
        let DEG=DegreeWorkOn.text
        let COML=ComputerLanguage.text
        
        let classbuilt = Student(FirstName: FN!, LastName: LN!, FromWhere: FW!, Gender: SEX!, Hobby: HB!, DegreeWorkOn: DEG!, ComputerLanguage: COML!)
        
        
        Output.text=classbuilt.description
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let DestViewController:V2ViewController=segue.destination as! V2ViewController
        DestViewController.ShowText=Output.text!
        DestViewController.PhotoImg=Photo.image
        
        
    }
    
    
    

    
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}




