//
//  StudentTableViewCell.swift
//  Project00
//
//  Created by Siyang Chen on 1/31/17.
//  Copyright © 2017 Siyang Chen. All rights reserved.
//

import UIKit

class StudentTableViewCell: UITableViewCell {

    //MARK:Attributes
    
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var Degree: UILabel!
    @IBOutlet weak var Photo: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
